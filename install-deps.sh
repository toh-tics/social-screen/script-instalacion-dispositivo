#!/bin/bash
{

PATHMONGO=/home/naika/.mongodb

checkOrInstallapt(){
    PKG_OK=$(dpkg-query -W --showformat='${Status}\n' $1|grep "install ok installed")
    echo Checking for $1
    if [ "" == "$PKG_OK" ]; then
       echo -e "\tNo $1 installed"
      sudo apt-get --force-yes install $1
    else
       echo -e "\t$1 Installed"
    fi
}

checkOrInstallaptitude(){
    PKG_OK=$(dpkg-query -W --showformat='${Status}\n' $1|grep "install ok installed")
    echo Checking for $1
    if [ "" == "$PKG_OK" ]; then
       echo -e "\tNo $1 installed"
       sudo aptitude --assume-yes install $1
    else
       echo -e "\t$1 Installed"
    fi
}

checkOrInstallnpm(){
    NPM_OK=$(npm -g ls $1)
    echo Checking for $1
    if [ "" == "$NPM_OK" ]; then
       echo -e "\tNo $1 installed"
       sudo npm install -g $1 $2
    else
       echo -e "\t$1 Installed"
    fi
}

purge_apt() {
    sudo apt-get purge $1* --assume-yes
    sudo apt-get clean --yes
    sudo apt-get autoremove --yes
}

purge_apt_deps() {
    sudo apt-get update
    purge_apt node
    purge_apt nodejs
    purge_apt libreoffice
    purge_apt iceweasel
    purge_apt firefox
    purge_apt chromium
    purge_apt vlc
    purge_apt gimp
    purge_apt geany
    purge_apt samba
    purge_apt mousepad
    purge_apt xscreensaver
    purge_apt xfburn
    purge_apt xfce4-notes
    purge_apt xfce4-clipman
    purge_apt gnome-orca
}

install_chrome() {
    wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
    sudo sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google.list'
    sudo apt-get update
    checkOrInstallaptitude google-chrome-stable
    sudo rm -f "/etc/apt/sources.list.d/google.list"
}

install_apt_deps() {
    sudo apt-get update
    checkOrInstallapt aptitude
    NODE_OK=$(which node)
    echo Checking for node
    if [ "" == "$NODE_OK" ]; then
        echo -e "\tNo node installed"
        sudo ln -s /usr/bin/nodejs /usr/bin/node
        #curl https://npmjs.org/install.sh | sudo sh
        wget -q -O - https://deb.nodesource.com/setup_4.x | sudo -E bash -
    else
        echo -e "\tnode Installed"
    fi


    checkOrInstallaptitude plymouth
    checkOrInstallaptitude plymouth-themes
    checkOrInstallaptitude curl
    ### Oculta el cursor ###
    checkOrInstallaptitude unclutter
    ### ScreenShots ###
    checkOrInstallaptitude scrot
    ### Configuracion de red ###
    checkOrInstallaptitude resolvconf
    ### Librerias para los torrents ###
    checkOrInstallaptitude python-pip
    checkOrInstallaptitude libtorrent-dev
    checkOrInstallaptitude libtorrent-rasterbar-dev
    checkOrInstallaptitude python-libtorrent
    #### Dependencias ###
    #checkOrInstallaptitude gcc
    #checkOrInstallaptitude g++
    checkOrInstallaptitude build-essential
    checkOrInstallaptitude cmake
    checkOrInstallaptitude make
    checkOrInstallaptitude pkg-config
    checkOrInstallaptitude libopencv-dev
    checkOrInstallaptitude imagemagick

    #checkOrInstallaptitude automake
    checkOrInstallaptitude firmware-realtek
    checkOrInstallaptitude wireless-tools
    ### Dependencias de la App ###
    checkOrInstallaptitude git
    checkOrInstallaptitude nodejs
    checkOrInstallaptitude npm
    checkOrInstallnpm node-gyp
    checkOrInstallaptitude mongodb
    #checkOrInstallaptitude chromium
    install_chrome

    sudo apt-get clean
    sudo apt-get autoremove --yes
    
    sudo apt-get install gedit
    sudo pip install pyserial
    #sudo chown -R `whoami` ~/.npm ~/tmp/
}

prepare_mongo_folder() {
    mkdir -p $PATHMONGO
    chown mongodb:mongodb $PATHMONGO
    chmod u+rwx,g-w+rx,o-w+rx $PATHMONGO
    echo Cambiar path de mongodb.conf a  $PATHMONGO
}

purge_apt_deps
install_apt_deps
prepare_mongo_folder

}