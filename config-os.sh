#!/bin/bash

{

###############################
### Rubén Ignacio Couoh Ku. ###
###############################

GRUB_FILE=/etc/default/grub
LIGHTDM_FILE=/etc/lightdm/lightdm.conf
POWER_SAVE_FILE=/etc/udev/rules.d/70-wifi-powersave.rules
MODPROBE_FILE=/etc/modprobe.d/rtl8723be.conf
BLACKLIST_FILE=/etc/modprobe.d/blacklist.conf


svs_print_red() {

  local RED
  RED='\033[0;31m'
  svs_print $RED$*
}

svs_print_green() {

  local GREEN
  GREEN='\033[0;32m'
  svs_print $GREEN$*
}

svs_print_orange() {

  local ORANGE
  ORANGE='\033[0;33m'
  svs_print $ORANGE$*
}

svs_print_blue() {

  local GREEN
  GREEN='\033[0;34m'
  svs_print $GREEN$*
}

svs_print_red() {

  local RED
  RED='\033[0;31m'
  svs_print $RED$*
}

svs_print() {

  local NC
  local MESSAGE

  MESSAGE=$*
  NC='\033[0m'

  echo -e $MESSAGE$NC
}

svs_configure_grub() {

### https://wiki.archlinux.org/index.php/LightDM_(Espa%C3%B1ol)

svs_print_green "[+] Creando respaldo del GRUB en ${GRUB_FILE}-$(date +'%d-%m%Y').back"
command cp $GRUB_FILE "${GRUB_FILE}-$(date +'%d-%m%Y').back"

svs_print_green "[+] Configurando GRUB..."
cat << EOF > $GRUB_FILE
# If you change this file, run 'update-grub' afterwards to update
# /boot/grub/grub.cfg.
# For full documentation of the options in this file, see:
#   info -f grub -n 'Simple configuration'

GRUB_DEFAULT=0
GRUB_HIDDEN_TIMEOUT=0
GRUB_TIMEOUT=0
GRUB_DISTRIBUTOR=`lsb_release -i -s 2> /dev/null || echo Debian`
#GRUB_CMDLINE_LINUX_DEFAULT="quiet"
GRUB_CMDLINE_LINUX_DEFAULT="quiet splash"
#GRUB_CMDLINE_LINUX_DEFAULT="quiet splash nomodeset video=HDMI-A-1:e"
GRUB_CMDLINE_LINUX=""
#GRUB_BACKGROUND="/home/naika"
# Uncomment to enable BadRAM filtering, modify to suit your needs
# This works with Linux (no patch required) and with any kernel that obtains
# the memory map information from GRUB (GNU Mach, kernel of FreeBSD ...)
#GRUB_BADRAM="0x01234567,0xfefefefe,0x89abcdef,0xefefefef"

# Uncomment to disable graphical terminal (grub-pc only)
#GRUB_TERMINAL=console

# The resolution used on graphical terminal
# note that you can use only modes which your graphic card supports via VBE
# you can see them in real GRUB with the command 'vbeinfo'
#GRUB_GFXMODE=640x480
GRUB_GFXMODE=1024x768

# Uncomment if you don't want GRUB to pass "root=UUID=xxx" parameter to Linux
#GRUB_DISABLE_LINUX_UUID=true

# Uncomment to disable generation of recovery mode menu entries
#GRUB_DISABLE_RECOVERY="true"

# Uncomment to get a beep at grub start
#GRUB_INIT_TUNE="480 440 1"
EOF

  svs_print_green "[+] Ejecutando update-grub."
  command sudo update-grub || {
    svs_print_red "[-] No se puedo actualizar el grub."
    svs_print_red "[-] Ejecute manualmente update-grub."
  }
}

svs_configure_lighdm() {

svs_print_green "[+] Creando respaldo de LIGHTDM en ${LIGHTDM_FILE}-$(date +'%d-%m-%Y').back"
command cp "${LIGHTDM_FILE}" "${LIGHTDM_FILE}-$(date +'%d-%m-%Y').back"

local _AUTOLOGIN_USER
read -p "Autologin-user: (${SUDO_USER:-${USER:-naika}}) < " _AUTOLOGIN_USER </dev/tty
_AUTOLOGIN_USER=${_AUTOLOGIN_USER:-${SUDO_USER:-${USER:-naika}}}
svs_print_blue "Autologin-user: ${_AUTOLOGIN_USER}"

svs_print_green "[+] Configurando LIGHTDM..."
cat << EOF > $LIGHTDM_FILE
#
# General configuration
#
# start-default-seat = True to always start one seat if none are defined in the configuration
# greeter-user = User to run greeter as
# minimum-display-number = Minimum display number to use for X servers
# minimum-vt = First VT to run displays on
# lock-memory = True to prevent memory from being paged to disk
# user-authority-in-system-dir = True if session authority should be in the system location
# guest-account-script = Script to be run to setup guest account
# logind-load-seats = True to automatically set up multi-seat configuration from logind
# logind-check-graphical = True to on start seats that are marked as graphical by logind
# log-directory = Directory to log information to
# run-directory = Directory to put running state in
# cache-directory = Directory to cache to
# sessions-directory = Directory to find sessions
# remote-sessions-directory = Directory to find remote sessions
# greeters-directory = Directory to find greeters
#
[LightDM]
#start-default-seat=true
#greeter-user=lightdm
#minimum-display-number=0
#minimum-vt=7
#lock-memory=true
#user-authority-in-system-dir=false
#guest-account-script=guest-account
#logind-load-seats=false
#logind-check-graphical=false
#log-directory=/var/log/lightdm
#run-directory=/var/run/lightdm
#cache-directory=/var/cache/lightdm
#sessions-directory=/usr/share/lightdm/sessions:/usr/share/xsessions
#remote-sessions-directory=/usr/share/lightdm/remote-sessions
#greeters-directory=/usr/share/lightdm/greeters:/usr/share/xgreeters

#
# Seat defaults
#
# type = Seat type (xlocal, xremote)
# xdg-seat = Seat name to set pam_systemd XDG_SEAT variable and name to pass to X server
# pam-service = PAM service to use for login
# pam-autologin-service = PAM service to use for autologin
# pam-greeter-service = PAM service to use for greeters
# xserver-command = X server command to run (can also contain arguments e.g. X -special-option)
# xserver-layout = Layout to pass to X server
# xserver-config = Config file to pass to X server
# xserver-allow-tcp = True if TCP/IP connections are allowed to this X server
# xserver-share = True if the X server is shared for both greeter and session
# xserver-hostname = Hostname of X server (only for type=xremote)
# xserver-display-number = Display number of X server (only for type=xremote)
# xdmcp-manager = XDMCP manager to connect to (implies xserver-allow-tcp=true)
# xdmcp-port = XDMCP UDP/IP port to communicate on
# xdmcp-key = Authentication key to use for XDM-AUTHENTICATION-1 (stored in keys.conf)
# unity-compositor-command = Unity compositor command to run (can also contain arguments e.g. unity-system-compositor -special-option)
# unity-compositor-timeout = Number of seconds to wait for compositor to start
# greeter-session = Session to load for greeter
# greeter-hide-users = True to hide the user list
# greeter-allow-guest = True if the greeter should show a guest login option
# greeter-show-manual-login = True if the greeter should offer a manual login option
# greeter-show-remote-login = True if the greeter should offer a remote login option
# user-session = Session to load for users
# allow-user-switching = True if allowed to switch users
# allow-guest = True if guest login is allowed
# guest-session = Session to load for guests (overrides user-session)
# session-wrapper = Wrapper script to run session with
# greeter-wrapper = Wrapper script to run greeter with
# guest-wrapper = Wrapper script to run guest sessions with
# display-setup-script = Script to run when starting a greeter session (runs as root)
# display-stopped-script = Script to run after stopping the display server (runs as root)
# greeter-setup-script = Script to run when starting a greeter (runs as root)
# session-setup-script = Script to run when starting a user session (runs as root)
# session-cleanup-script = Script to run when quitting a user session (runs as root)
# autologin-guest = True to log in as guest by default
# autologin-user = User to log in with by default (overrides autologin-guest)
# autologin-user-timeout = Number of seconds to wait before loading default user
# autologin-session = Session to load for automatic login (overrides user-session)
# autologin-in-background = True if autologin session should not be immediately activated
# exit-on-failure = True if the daemon should exit if this seat fails
#
[SeatDefaults]
#type=xlocal
#xdg-seat=seat0
pam-service=lightdm
pam-autologin-service=lightdm-autologin
#pam-greeter-service=lightdm-greeter
#xserver-command=X
#xserver-layout=
#xserver-config=
#xserver-allow-tcp=false
#xserver-share=true
#xserver-hostname=
#xserver-display-number=
#xdmcp-manager=
#xdmcp-port=177
#xdmcp-key=
#unity-compositor-command=unity-system-compositor
#unity-compositor-timeout=60
#greeter-session=example-gtk-gnome
#greeter-hide-users=false
#greeter-allow-guest=true
#greeter-show-manual-login=false
#greeter-show-remote-login=true
#user-session=default
#allow-user-switching=true
#allow-guest=true
#guest-session=
#session-wrapper=lightdm-session
#greeter-wrapper=
#guest-wrapper=
#display-setup-script=
#display-stopped-script=
#greeter-setup-script=
#session-setup-script=
#session-cleanup-script=
#autologin-guest=false
autologin-user=$_AUTOLOGIN_USER
autologin-user-timeout=0
#autologin-in-background=false
#autologin-session=UNIMPLEMENTED
#exit-on-failure=false

#
# Seat configuration
#
# Each seat must start with "Seat:".
# Uses settings from [SeatDefaults], any of these can be overriden by setting them in this section.
#
#[Seat:0]

#
# XDMCP Server configuration
#
# enabled = True if XDMCP connections should be allowed
# port = UDP/IP port to listen for connections on
# key = Authentication key to use for XDM-AUTHENTICATION-1 or blank to not use authentication (stored in keys.conf)
#
# The authentication key is a 56 bit DES key specified in hex as 0xnnnnnnnnnnnnnn.  Alternatively
# it can be a word and the first 7 characters are used as the key.
#
[XDMCPServer]
#enabled=false
#port=177
#key=

#
# VNC Server configuration
#
# enabled = True if VNC connections should be allowed
# command = Command to run Xvnc server with
# port = TCP/IP port to listen for connections on
# width = Width of display to use
# height = Height of display to use
# depth = Color depth of display to use
#
[VNCServer]
#enabled=false
#command=Xvnc
#port=5900
#width=1024
#height=768
#depth=8
EOF
}

### Requiere reinicio.
svs_configure_power_save() {

svs_print_green "[+] Deshabilitando el ahorro de energia en la interfaz de red..."
cat << EOF> $POWER_SAVE_FILE
ACTION=="add", SUBSYSTEM=="net", KERNEL=="wlan*" RUN+="/sbin/iw dev %k set power_save off"
EOF

svs_print_green "[+] Configurando el modulo rtl8723be."
cat << EOF > $MODPROBE_FILE
options rtl8723be fwlps=N ips=N
EOF

  command sudo update-initramfs -u -k "$(uname -r)" || {
    svs_print_red "[-] Ejecute manualmente update-initramfs."
  }
}

### Requiere reinicio.
svs_configure_blacklist() {

  svs_print_green "[+] Creando respaldo de blacklist en ${BLACKLIST_FILE}-$(date +'%d-%m%Y').back"
  command cp $BLACKLIST_FILE "${BLACKLIST_FILE}-$(date +'%d-%m%Y').back"

  svs_print_green "[+] Configurando ${BLACKLIST_FILE}"

cat << EOF> $BLACKLIST_FILE
#blacklist usbhid
blacklist usb-storage
blacklist usb_storage
EOF

  command sudo update-initramfs -u -k "$(uname -r)" || {
    svs_print_red "[-] Ejecute manualmente update-initramfs."
  }
}

svs_generic_configuration() {

  svs_print_green "[+] Aplicando configuraciones genericas."
  command sudo rm /etc/xdg/autostart/gnome-keyring*
  command sudo rm /etc/xdg/autostart/xscreensaver.desktop

  command sudo chmod -x /usr/bin/gnome-keyring
  command sudo chmod -x /usr/bin/gnome-keyring-3
  command sudo chmod -x /usr/bin/gnome-keyring-daemon
  command update-alternatives --set gnome-www-browser /usr/bin/google-chrome-stable
  command update-alternatives --set x-www-browser /usr/bin/google-chrome-stable
}



svs_clean_tmp_files() {

  local TMP_CONFIG_FILE
  TMP_CONFIG_FILE=/usr/lib/tmpfiles.d/tmp.conf

  svs_print_green "[+] Configurando eliminación de archivos temporales."

cat << EOF> $TMP_CONFIG_FILE
#  This file is part of systemd.
#
#  systemd is free software; you can redistribute it and/or modify it
#  under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation; either version 2.1 of the License, or
#  (at your option) any later version.

# See tmpfiles.d(5) for details

# Clear tmp directories separately, to make them easier to override
#D /tmp 1777 root root -
#D /tmp 1777 root root 7d
#d /var/tmp 1777 root root 30d
d /tmp 1777 root root 3d
d /var/tmp 1777 root root 3d

# Exclude namespace mountpoints created with PrivateTmp=yes
# x /tmp/systemd-private-%b-*
# X /tmp/systemd-private-%b-*/tmp
# x /var/tmp/systemd-private-%b-*
# X /var/tmp/systemd-private-%b-*/tmp
EOF
}

svs_main() {

  # Deshabilita las opciones del GRUB al iniciar el S.O.
  svs_configure_grub

  # Activa el inicio de sesión automático.
  svs_configure_lighdm

  # Deshabilita el ahorro de energia en las interfaces de red.
  svs_configure_power_save

  # Deshabilita los modulos usb.
  svs_configure_blacklist

  #
  svs_generic_configuration

  # Limpieza de archivos temporales
  svs_clean_tmp_files

  # Reinicio.
  svs_print_orange "[*] Se recomienda reiniciar el dispositivo.\n"

  while true; do
    read -p "Desea reiniciar el dispositivo? yes | no: " yn </dev/tty
    case $yn in
      yes)
      svs_print_green "[*] Reiniciando el dispositivo en 10 segundos..."
      sleep 10
      sudo reboot
      ;;
      no)
      svs_print_green "[*] Configuración finalizada."
      break
      ;;
      *)
      svs_print_orange "[*] Ingrese yes \| no"
      ;;
    esac
  done

}

svs_main

###
unset svs_print \
      svs_print_red \
      svs_print_green \
      svs_print_blue \
      svs_print_orange \
      svs_configure_grub \
      svs_configure_lighdm \
      svs_configure_power_save \
      svs_configure_blacklist \
      svs_generic_configuration \
      svs_clean_tmp_files \
      svs_main
}